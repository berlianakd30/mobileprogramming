//No. 1
function teriak() {
    console.log("Halo Humanika!");
}
teriak();

//No. 2
function kalikan(num1_param, num2_param) {
    return num1_param * num2_param;
  }
  
    var num1 = 12;
    var num2 = 4;
    
    var hasilPerkalian = kalikan(num1,num2);
    console.log(hasilPerkalian); // Menampilkan angka 48

//No. 3
function introduce(name, age, address, hobby) {
    return("Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby);
}
    var name = "Agus" 
    var age = 30 
    var address = "Jln. Malioboro, Yogyakarta" 
    var hobby = "Gaming!" 
    var perkenalan = introduce(name, age, address, hobby) 
    console.log(perkenalan) // Menampilkan "Nama saya Agus, umur saya 30  tahun, alamat saya di Jln. Malioboro, Yogyakarta, dan saya punya  hobby yaitu Gaming!"
